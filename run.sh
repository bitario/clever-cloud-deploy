#!/bin/bash
echo "Starting Clever Cloud deployment..."
# check if slack webhook url is present
if [ -z "$WERCKER_CLEVER_CLOUD_DEPLOY_URL" ]; then
  fail "Please provide a URL"
fi

if [ -z "$WERCKER_CLEVER_CLOUD_DEPLOY_SSH" ]; then
  fail "Please provide a SSH key"
fi

function __wget() {
    : ${DEBUG:=0}
    local URL=$1
    local tag="Connection: close"
    local mark=0

    if [ -z "${URL}" ]; then
        printf "Usage: %s \"URL\" [e.g.: %s http://www.google.com/]" \
               "${FUNCNAME[0]}" "${FUNCNAME[0]}"
        return 1;
    fi
    read proto server path <<<$(echo ${URL//// })
    DOC=/${path// //}
    HOST=${server//:*}
    PORT=${server//*:}
    [[ x"${HOST}" == x"${PORT}" ]] && PORT=80
    [[ $DEBUG -eq 1 ]] && echo "HOST=$HOST"
    [[ $DEBUG -eq 1 ]] && echo "PORT=$PORT"
    [[ $DEBUG -eq 1 ]] && echo "DOC =$DOC"

    exec 3<>/dev/tcp/${HOST}/$PORT
    echo -en "GET ${DOC} HTTP/1.1\r\nHost: ${HOST}\r\n${tag}\r\n\r\n" >&3
    while read line; do
        [[ $mark -eq 1 ]] && echo $line
        if [[ "${line}" =~ "${tag}" ]]; then
            mark=1
        fi
    done <&3
    exec 3>&-
}

gitssh_path="";
ssh_key_path="";
first_row=true;
working_directory="/pipeline/source";
branch="master";

gitssh_path="$(mktemp)";
ssh_key_path="$(mktemp -d)/id_rsa";

ssh -o StrictHostKeyChecking=no git@push.par.clever-cloud.com uptime

git config --global user.email "admin@bitario.nl"
git config --global user.name "Wercker"

echo -e "$WERCKER_CLEVER_CLOUD_DEPLOY_SSH" > "$ssh_key_path";
chmod 0600 "$ssh_key_path";

echo "ssh -e none -i \"$ssh_key_path\" \$@" > "$gitssh_path";
chmod 0700 "$gitssh_path";
export GIT_SSH="$gitssh_path";

__wget $WERCKER_CLEVER_CLOUD_DEPLOY_URL | while read line
do

	if $first_row; then
		first_row=false;
	else
		echo ""
		echo "GIT URL: $line"
		
		echo "Initializing new git repo..."
		rm -rf .git		
		git init
		
		echo "Creating local master branch..."
		git checkout master
		git add .
		git commit -m 'wercker deploy'
		
		echo "Adding clevercloud remote..."
		git remote add clevercloud $line;
		
		echo "Merging in master branch..."
		git pull clevercloud/master -s recursive -X ours
		
		echo "Pushing to clevercloud..."
		git push -f clevercloud HEAD:refs/heads/master;
		git remote rm clevercloud
	fi   
done
